<?php

namespace BoostMyShop\AdvancedStock\Model\Config\Source\Discrepency;

class StockItemQuantityFixMethod implements \Magento\Framework\Option\ArrayInterface
{
    const kCreateStockMovement = 'create_stock_movement';
    const kUpdateQuantityFromStockMovement = 'update_quantity_from_stock_movement';

    public function toOptionArray()
    {
        $options = array();

        $options[] = array('value' => self::kCreateStockMovement, 'label' => 'Create stock movements');
        $options[] = array('value' => self::kUpdateQuantityFromStockMovement, 'label' => 'Update quantity from stock movements');

        return $options;
    }


}
