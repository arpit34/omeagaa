<?php

namespace BoostMyShop\AdvancedStock\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

use Magento\CatalogInventory\Api\StockItemRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;

class CatalogProductAfterSave implements ObserverInterface
{

    protected $_warehouseItemFactory;
    protected $_warehouseCollectionFactory;
    protected $_stockMovementFactory;
    protected $_backendAuthSession;
    protected $_logger;
    protected $_stockHelper;

    protected $_stockRegistry;
    protected $_stockItemRepository;


    /**
     * @param StockIndexInterface $stockIndex
     * @param StockConfigurationInterface $stockConfiguration
     * @param StockRegistryInterface $stockRegistry
     * @param StockItemRepositoryInterface $stockItemRepository
     */
    public function __construct(
        \BoostMyShop\AdvancedStock\Helper\Logger $logger,
        \BoostMyShop\AdvancedStock\Model\Warehouse\ItemFactory $warehouseItemFactory,
        \BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\CollectionFactory $warehouseCollectionFactory,
        \Magento\Backend\Model\Auth\Session $backendAuthSession,
        \BoostMyShop\AdvancedStock\Model\ResourceModel\CatalogInventory\Stock $stockHelper,
        \BoostMyShop\AdvancedStock\Model\StockMovementFactory $stockMovementFactory,
        StockRegistryInterface $stockRegistry,
        StockItemRepositoryInterface $stockItemRepository
    ) {
        $this->_warehouseItemFactory = $warehouseItemFactory;
        $this->_warehouseCollectionFactory = $warehouseCollectionFactory;
        $this->_stockMovementFactory = $stockMovementFactory;
        $this->_backendAuthSession = $backendAuthSession;
        $this->_logger = $logger;
        $this->_stockHelper = $stockHelper;

        $this->_stockRegistry = $stockRegistry;
        $this->_stockItemRepository = $stockItemRepository;
    }

    /**
     * Saving product inventory data. Product qty calculated dynamically.
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $product = $observer->getEvent()->getProduct();
        if (!$product)
            return;

        //if product just created
        if (!$product->getOrigData('entity_id')) {

            foreach($this->getWarehouseIds() as $warehouseId)
                $this->_warehouseItemFactory->create()->createRecord($product->getId(), $warehouseId);

            $defaultStockItem = $this->_stockRegistry->getStockItem($product->getId(), 0);

            foreach($this->_stockHelper->listStocks() as $stock)
            {
                $this->_logger->log('Create stock item for product#'.$product->getId().', stock #'.$stock['stock_id'].' website #'.$stock['website_id']);

                $stockItem = $this->_stockRegistry->getStockItem($product->getId(), $stock['website_id']);
                $stockItemData = $defaultStockItem->getData();
                unset($stockItemData['item_id']);
                $stockItemData['product_id'] = $product->getId();
                $stockItemData['stock_id'] = $stock['stock_id'];
                $stockItemData['website_id'] = $stock['website_id'];

                $stockItem->addData($stockItemData);
                $this->_stockItemRepository->save($stockItem);

            }

            if (isset($product->getData()['stock_data']['qty']))
            {
                $this->_logger->log('Create Stock movement for  product#'.$product->getId().' to initialize quantity & stock movement after product creation ', \BoostMyShop\AdvancedStock\Helper\Logger::kLogInventory);

                $qty = $product->getData()['stock_data']['qty'];
                $this->_stockMovementFactory->create()->updateProductQuantity($product->getId(), 1, 0, $qty, 'Product creation', $this->getUserId());
            }

        }

        return $this;
    }

    protected function getWarehouseIds()
    {
        return $this->_warehouseCollectionFactory->create()->getAllIds();
    }

    protected function getUserId()
    {
        $userId = null;
        if ($this->_backendAuthSession->getUser())
            $userId = $this->_backendAuthSession->getUser()->getId();
        return $userId;
    }
}
